/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Interfaz.Bienvenida;
import Interfaz.Compras;
import Interfaz.Ventas;
//import Interfaz.Verificacion;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CCNAR
 */
public class ControladorCompras implements ActionListener, MouseListener, KeyListener {
    
    Compras agr;
    
    public ControladorCompras(Compras agr) {
        this.agr = agr;
        this.agr.btn_Eliminar.addActionListener(this);
        this.agr.btn_IngresarPro.addActionListener(this);
        this.agr.btn_modificar.addActionListener(this);
        this.agr.tb_Productos.addMouseListener(this);
        this.agr.tb_Compra.addMouseListener(this);
        this.agr.txt_idProducto.requestFocus(true);
        this.agr.btn_Eliminar.addMouseListener(this);
        this.agr.btn_IngresarPro.addMouseListener(this);
        this.agr.btn_modificar.addMouseListener(this);
        this.agr.txt_idProducto.addKeyListener(this);
        this.agr.txt_idVenta.addKeyListener(this);
        this.agr.btn_nuevo.addActionListener(this);
        this.agr.txt_Producto.addKeyListener(this);
        this.agr.txt_proveedor.addKeyListener(this);
        this.agr.btn_pagar.addActionListener(this);
        //  colocarDatos();
        ActualizarId();
        cargarTabla("", "");
//        LLenarCBX();
        Botones(false, false);
        sumaTotal();
        
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == agr.btn_IngresarPro) {
            if (camposVacios() == true) {
                agr.txt_idProducto.requestFocus(true);
            } else {
                int idproducto = Integer.parseInt(agr.txt_idProducto.getText());
                if (verificarProducto(idproducto) == true) {
                    Ingresar();
                    cargarCompras();
                    sumaTotal();
                    Limpiar();
                } else {
                    agr.txt_idProducto.requestFocus(true);
                    JOptionPane.showMessageDialog(null, "El id del Producto no se encuentra en la Base de Datos", "Verificar ID_Producto", JOptionPane.ERROR_MESSAGE);
                }
            }
        } else if (e.getSource() == agr.btn_modificar) {
            if (camposVacios() == true) {
                agr.txt_idProducto.requestFocus(true);
            } else {
                Modificar();
                cargarCompras();
                Limpiar();
                sumaTotal();
            }
        } else if (e.getSource() == agr.btn_nuevo) {
            Limpiar();
            Botones(false, true);
            buscarUser(agr.txt_user.getText());
        } else if (e.getSource() == agr.btn_pagar) {
            ActualizarId();
            pagar();
            Limpiar();
            cargarCompras();
            sumaTotal();
            
        }
        
    }
    
    public void colocarDatos() {
        Bienvenida bv = new Bienvenida();
        String user = bv.txt_user.getText();
        String cargo = bv.txt_cargo.getText();
        System.out.println("user " + user);
        agr.txt_user.setText(user);
        agr.txt_cargo.setText(cargo);
    }
    
    public boolean camposVacios() {
        if (agr.txt_idProducto.getText().isEmpty() || agr.txt_Producto.getText().isEmpty() || agr.txt_cantidad.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Campos Vacios ", "Campos Vacios", JOptionPane.ERROR_MESSAGE);
            return true;
        } else {
            return false;
        }
    }
    
    public boolean verificarProducto(int idProd) {
        try {
            String prodAux = "";
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT Producto FROM db_Productos WHERE id = ?");
            ps.setInt(1, idProd);
            rs = ps.executeQuery();
            while (rs.next()) {
                prodAux = (rs.getString("Producto"));                
            }
            if (prodAux.isEmpty()) {
                agr.txt_Producto.setText(prodAux);
                return false;
            } else {
                agr.txt_Producto.setText(prodAux);
                return true;
            }
        } catch (Exception e) {
            System.out.println("error Verificar prod " + e.getMessage());
        }
        return false;
    }
    
    public void Ingresar() {
        int idVentas = Integer.parseInt(agr.txt_idVenta.getText());
        String Año = agr.txt_AÑO.getText();
        String MES = agr.txt_Mes.getText();
        String FECHA = agr.txt_fecha.getText();
        int idProducto = Integer.parseInt(agr.txt_idProducto.getText());
        String Producto = agr.txt_Producto.getText();
        float precio_venta = Float.parseFloat(agr.txt_precio.getText());
        int cantidad = Integer.parseInt(agr.txt_cantidad.getText());
        String user = agr.txt_user.getText();
        int idUsuario = buscarUser(user);
        String Proveedor = agr.txt_proveedor.getText();
        int idProveedor = buscarProveedor(Proveedor);
        float subtotal = cantidad * precio_venta;
        float total = subtotal + ((subtotal * 16) / 100);
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("EXECUTE SP_Compras ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?");
            ps.setInt(1, idVentas);
            ps.setString(2, Año);
            ps.setString(3, MES);
            ps.setString(4, FECHA);
            ps.setString(5, Producto);
            ps.setInt(6, idProducto);
            ps.setInt(7, cantidad);
            ps.setString(8, user);
            ps.setInt(9, idUsuario);
            ps.setString(10, Proveedor);
            ps.setInt(11, idProveedor);
            ps.setFloat(12, precio_venta);
            ps.setFloat(13, subtotal);
            ps.executeUpdate();
            cargarTabla("", "");
            JOptionPane.showMessageDialog(null, "Se Ingreso la Compra correctamente");
        } catch (Exception e) {
            System.out.println("error ingresar " + e.getMessage());
        }
    }
    
    public void Modificar() {
        int idVentas = Integer.parseInt(agr.txt_idVenta.getText());
        String Año = agr.txt_AÑO.getText();
        String MES = agr.txt_Mes.getText();
        String FECHA = agr.txt_fecha.getText();
        int idProducto = Integer.parseInt(agr.txt_idProducto.getText());
        String Producto = agr.txt_Producto.getText();
        float precio_venta = Float.parseFloat(agr.txt_precio.getText());
        int cantidad = Integer.parseInt(agr.txt_cantidad.getText());
        int cantidaAux = Integer.parseInt(agr.txt_AuxCant.getText());
        String user = agr.txt_user.getText();
        int idUsuario = buscarUser(user);
        String Proveedor = agr.txt_proveedor.getText();
        int idProveedor = buscarProveedor(Proveedor);
        float subtotal = cantidad * precio_venta;
        float total = subtotal + ((subtotal * 16) / 100);
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("EXECUTE SP_ModificarCompras ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?");
            ps.setInt(1, idVentas);
            ps.setString(2, Año);
            ps.setString(3, MES);
            ps.setString(4, FECHA);
            ps.setString(5, Producto);
            ps.setInt(6, idProducto);
            ps.setInt(7, cantidad);
            ps.setInt(8, cantidaAux);
            ps.setString(9, user);
            ps.setInt(10, idUsuario);
            ps.setString(11, Proveedor);
            ps.setInt(12, idProveedor);
            ps.setFloat(13, precio_venta);
            ps.setFloat(14, subtotal);
            ps.executeUpdate();
            cargarTabla("", "");
            JOptionPane.showMessageDialog(null, "Se Modifico la Compra correctamente");
        } catch (Exception e) {
            System.out.println("error ingresar " + e.getMessage());
        }
    }
    
    public void EliminarRegistro(Compras cm) {
        if (cm.txt_op.getText().equals("SI")) {
            int idProducto = Integer.parseInt(cm.txt_idProducto.getText());
            int idVenta = Integer.parseInt(cm.txt_idVenta.getText());
            int Cantidad = Integer.parseInt(cm.txt_cantidad.getText());
            String Producto = agr.txt_Producto.getText();
            try {
                Connection conectar = Conexion.establecerConnection();
                PreparedStatement ps = conectar.prepareStatement("EXECUTE SP_EliminarCompra ?,?,?");
                ps.setString(1, Producto);
                ps.setInt(2, Cantidad);
                ps.setInt(3, idVenta);
                ps.executeUpdate();
                cargarTabla("", "");
                cargarCompras();
                JOptionPane.showMessageDialog(null, "Se Elimino el registro seleccionado correctamente");
            } catch (Exception e) {
                System.out.println("error: " + e.getMessage());
            }
        } else {
            
        }
        
    }
    
    public void sumaTotal() {
        float subtotal = 0;
        float total = 0;
        int idVenta;
        if (agr.txt_idVenta.getText().isEmpty()) {
            idVenta = 0;
        } else {
            idVenta = Integer.parseInt(agr.txt_idVenta.getText());
        }
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT SUM(Total_Compra) AS total FROM db_Compras WHERE id_compra = ?");
            ps.setInt(1, idVenta);
            rs = ps.executeQuery();
            while (rs.next()) {
                total = (rs.getInt("total"));                
            }
            agr.TxtTOTAL.setText("" + total);
        } catch (Exception e) {
            System.out.println("error en Buscar User " + e.getMessage());
        }
    }
    
    public int buscarUser(String user) {
        int idAux = 0;
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT id_Usuario  FROM db_Usuarios WHERE Usuario = ?");
            ps.setString(1, user);
            rs = ps.executeQuery();
            while (rs.next()) {
                idAux = (rs.getInt("id_Usuario"));                
                
            }
            return idAux;
        } catch (Exception e) {
            System.out.println("error en Buscar User " + idAux);
        }
        return idAux;
    }
    
    public int buscarProveedor(String Prov) {
        int idAux = 0;
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT id_Proveedor  FROM db_Proveedor WHERE Proveedor = ?");
            ps.setString(1, Prov);
            rs = ps.executeQuery();
            while (rs.next()) {
                idAux = (rs.getInt("id_Proveedor"));                
                
            }
            return idAux;
        } catch (Exception e) {
            System.out.println("error en Buscar Proveedor " + idAux);
        }
        return idAux;
    }
    
    public void pagar() {
        int idVenta = Integer.parseInt(agr.txt_idVenta.getText());
        String estado = "Pagado";
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("UPDATE db_Compras SET Estado = ? WHERE id_compra = ?");
            ps.setString(1, estado);
            ps.setInt(2, idVenta);
            ps.executeUpdate();
            cargarTabla("", "");
            cargarCompras();
            JOptionPane.showMessageDialog(null, "SE CONCLUYO LA COMPRA");
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
        }
    }
    
    public void ActualizarId() {
        int idAux = 0;
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT MAX(id_compra) AS id_Maximo FROM db_Compras");
            rs = ps.executeQuery();
            while (rs.next()) {
                idAux = (rs.getInt("id_Maximo") + 1);                
            }
            agr.txt_idVenta.setText("" + idAux);
        } catch (Exception e) {
        }
    }
    
    public void Botones(boolean ver, boolean ver2) {
        agr.btn_Eliminar.setEnabled(ver);
        agr.btn_modificar.setEnabled(ver);
        agr.btn_IngresarPro.setEnabled(ver2);
    }

//    public void LLenarCBX() {
//        try {
//            String Estado = "Activo";
//            agr.cbx_Proveedor.removeAllItems();
//            PreparedStatement ps;
//            ResultSet rs;
//            Connection con = Conexion.establecerConnection();
//            ps = con.prepareStatement("SELECT Proveedor FROM db_Proveedor WHERE Estado = ?");
//            ps.setString(1, Estado);
//            rs = ps.executeQuery();
//            int i = 0;
//            while (rs.next()) {
//                String Proveedor = rs.getString("Proveedor");
//                agr.cbx_Proveedor.addItem(Proveedor);
//                i++;
//            }
//
//        } catch (Exception er) {
//            System.err.println("Error en cbx Prov: " + er.toString());
//        }
//
//    }
    public void cargarTabla(String idProd, String info) {
        DefaultTableModel modeloTabla = (DefaultTableModel) agr.tb_Productos.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        
        int id = 0;
        if (idProd.isEmpty()) {
            id = 0;
        } else {
            id = Integer.parseInt(idProd);
        }
        
        int columnas;
        int[] ancho = {50, 150, 90, 150, 150};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            agr.tb_Productos.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sqlID = "SELECT id, Producto, Cantidad, Precio_Compra, Proveedor FROM db_Productos WHERE id = ?";
            String vacio = "SELECT id, Producto, Cantidad, Precio_Compra, Proveedor FROM db_Productos ORDER BY id";
            String dato = "SELECT id, Producto, Cantidad, Precio_Compra, Proveedor FROM db_Productos WHERE Producto LIKE '%" + info + "%' "
                    + "OR Proveedor LIKE '%" + info + "%'";
            
            if ("".equalsIgnoreCase(idProd) && "".equalsIgnoreCase(info)) {
                ps = con.prepareStatement(vacio);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            } else if (!("".equalsIgnoreCase(info))) {
                ps = con.prepareStatement(dato);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            } else if (!("".equalsIgnoreCase(idProd)) && ("".equalsIgnoreCase(info))) {
                ps = con.prepareStatement(sqlID);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }
        } catch (Exception e) {
            System.err.println("Error en tabla CargarProd: " + e.toString());
        }
    }
    
    public void cargarCompras() {
        DefaultTableModel modeloTabla = (DefaultTableModel) agr.tb_Compra.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int id = 0;
        if (agr.txt_idVenta.getText().isEmpty()) {
            id = 0;
        } else {
            id = Integer.parseInt(agr.txt_idVenta.getText());
        }
        int columnas;
        int[] ancho = {90, 150, 90, 150, 150,100};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            agr.tb_Compra.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT id_compra, Producto, Cantidad, Precio_Compra, Total_Compra, Estado FROM db_Compras WHERE id_compra = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();            
            rsmd = rs.getMetaData();
            columnas = rsmd.getColumnCount();
            
            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
            }
        } catch (Exception e) {
            System.err.println("Error en tabla CargarComprasprod: " + e.toString());
        }
    }

    //******* EVENTO QUE AFECTA A LA TABLA QUE SE TIENE EN JAVA, PARA QUE SE PUEDA COLOCAR LOS DATOS DESEADOS EN LOS TXT
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == agr.tb_Productos) {
            try {
                int fila = agr.tb_Productos.getSelectedRow();
                int id = Integer.parseInt(agr.tb_Productos.getValueAt(fila, 0).toString());
                String Pro = agr.tb_Productos.getValueAt(fila, 1).toString();
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT id, Producto,  Precio_Compra, Proveedor FROM db_Productos WHERE id = ? AND Producto = ?");
                ps.setInt(1, id);
                ps.setString(2, Pro);
                rs = ps.executeQuery();
                while (rs.next()) {
                    agr.txt_idProducto.setText("" + rs.getInt("id"));
                    agr.txt_Producto.setText(rs.getString("Producto"));
                    agr.txt_precio.setText("" + rs.getFloat("Precio_Compra"));
                    agr.txt_proveedor.setText(rs.getString("Proveedor"));
//                    agr.txt_cliente.setText(rs.getString("Nombre_Cliente"));
                    
                }
//                if( agr.txt_estado.getText().equals("Inactivo")){
//                    agr.btn_Desactivar.setText("Reingresar");
//                }else if( agr.txt_estado.getText().equals("Activo")){
//                    agr.btn_Desactivar.setText("Desactivar");
//                }                 
                Botones(true, true);
                agr.txt_cantidad.requestFocus(true);
                Stock(agr.txt_idProducto.getText());
            } catch (Exception er) {
                System.err.println("Error en tabla prod mouse: " + er.toString());
            }
        } else if (e.getSource() == agr.tb_Compra) {
            try {
                int fila = agr.tb_Compra.getSelectedRow();
                int id = Integer.parseInt(agr.tb_Compra.getValueAt(fila, 0).toString());
                String Pro = agr.tb_Compra.getValueAt(fila, 1).toString();
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT id_compra, Producto, id_Producto,  Cantidad, Usuario, Proveedor,  Precio_Compra FROM db_Compras WHERE id_compra = ? AND Producto = ?");
                ps.setInt(1, id);
                ps.setString(2, Pro);
                rs = ps.executeQuery();
                while (rs.next()) {
                    agr.txt_idVenta.setText("" + rs.getInt("id_compra"));
                    agr.txt_idProducto.setText("" + rs.getInt("id_Producto"));
                    agr.txt_cantidad.setText("" + rs.getInt("Cantidad"));
                    agr.txt_AuxCant.setText("" + rs.getInt("Cantidad"));
                    agr.txt_Producto.setText(rs.getString("Producto"));
                    agr.txt_proveedor.setText("" + rs.getString("Proveedor"));
                    agr.txt_precio.setText("" + rs.getFloat("Precio_Compra"));
//                    agr.txt_cliente.setText(rs.getString("Nombre_Cliente"));
                    
                }
//                if( agr.txt_estado.getText().equals("Inactivo")){
//                    agr.btn_Desactivar.setText("Reingresar");
//                }else if( agr.txt_estado.getText().equals("Activo")){
//                    agr.btn_Desactivar.setText("Desactivar");
//                }                 
                Botones(true, false);
                Stock(agr.txt_idProducto.getText());
            } catch (Exception er) {
                System.err.println("Error en tabla prod mouse: " + er.toString());
            }
        }
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {
    }
    
    @Override
    public void mouseEntered(MouseEvent e) {
        if (e.getSource() == agr.btn_Eliminar) {
            agr.btn_Eliminar.setBackground(Color.black);
        } else if (e.getSource() == agr.btn_IngresarPro) {
            agr.btn_IngresarPro.setBackground(Color.black);
        } else if (e.getSource() == agr.btn_modificar) {
            agr.btn_modificar.setBackground(Color.black);
        }
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
        if (e.getSource() == agr.btn_Eliminar) {
            agr.btn_Eliminar.setBackground(Color.red);
        } else if (e.getSource() == agr.btn_IngresarPro) {
            agr.btn_IngresarPro.setBackground(Color.red);
        } else if (e.getSource() == agr.btn_modificar) {
            agr.btn_modificar.setBackground(Color.red);
        }
        
    }
    
    public void Stock(String id) {
        int stock = 0;
        int idP = 0;
        
        if (id.isEmpty()) {
            idP = 0;
        } else {
            idP = Integer.parseInt(agr.txt_idProducto.getText());
        }
        try {
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT Cantidad FROM db_Productos WHERE id = ?");
            ps.setInt(1, idP);
            rs = ps.executeQuery();
            while (rs.next()) {
                stock = (rs.getInt("Cantidad"));                
            }
            agr.Lb_Stock.setText("" + stock);
        } catch (Exception e) {
            System.out.println("error buscarProducto " + e.getMessage());
        }
    }
    
    public void Limpiar() {
        agr.txt_Producto.setText("");
        agr.txt_precio.setText("");
        agr.txt_idProducto.setText("");
        agr.txt_cantidad.setText("");
        agr.Lb_Stock.setText("");
        agr.txt_idProducto.requestFocus(true);
    }
    
    @Override
    public void keyTyped(KeyEvent e) {
    }
    
    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            if (e.getSource() == agr.txt_idProducto) {
                if (agr.txt_Producto.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Producto No Encontrado", "ID no Encontrado", JOptionPane.ERROR_MESSAGE);
                    agr.txt_idProducto.requestFocus(true);
                } else {
                    agr.txt_cantidad.requestFocus(true);                    
                }
            }
        }
    }
    
    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == agr.txt_idProducto) {
            if (agr.txt_idProducto.getText().isEmpty()) {
                Limpiar();
                cargarTabla("", "");
            } else {
                String id = agr.txt_idProducto.getText();
                cargarTabla(id, "");
                cargarCompras();
                buscarProducto(id);
                Stock(id);
                Botones(false, true);
            }
            
        } else if (e.getSource() == agr.txt_idVenta) {
            cargarCompras();
            sumaTotal();
        } else if (e.getSource() == agr.txt_Producto) {
            String prod = agr.txt_Producto.getText();
            cargarTabla("", prod);
        } else if (e.getSource() == agr.txt_proveedor) {
            String prov = agr.txt_proveedor.getText();
            cargarTabla("", prov);
        }
    }
    
    public void buscarProducto(String id) {
        int idpro = 0;
        if (id.isEmpty()) {
            idpro = 0;
        } else {
            idpro = Integer.parseInt(id);
        }
        try {
            String prodAux = "";
            float precio = 0;
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT Producto, Precio_Venta FROM db_Productos WHERE id = ?");
            ps.setInt(1, idpro);
            rs = ps.executeQuery();
            while (rs.next()) {
                prodAux = (rs.getString("Producto"));                
                precio = rs.getFloat("Precio_Venta");
            }
            agr.txt_Producto.setText(prodAux);
            agr.txt_precio.setText("" + precio);
        } catch (Exception e) {
            System.out.println("error buscarProducto " + e.getMessage());
        }
    }
    
}
