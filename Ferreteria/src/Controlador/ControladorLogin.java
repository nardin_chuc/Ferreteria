
package Controlador;

import Conexion.Conexion;
import Interfaz.Bienvenida;
import Interfaz.Login;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

public class ControladorLogin implements ActionListener, MouseListener, KeyListener{
    
    
    Login lg;
    String Cargo = "";
    String user2 = "";
    public ControladorLogin(Login lg) {
        this.lg = lg;
        this.lg.btn_Cancelar.addActionListener(this);
        this.lg.btn_Ingresar.addActionListener(this);
        this.lg.txt_Contra.addKeyListener(this);
        this.lg.btn_Ingresar.addKeyListener(this);
        this.lg.btn_Cancelar.addMouseListener(this);
        this.lg.btn_Ingresar.addMouseListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == lg.btn_Cancelar){
            int op = JOptionPane.showConfirmDialog(null, "¿ Desea Salir ?", "Salir", JOptionPane.INFORMATION_MESSAGE);
            if(op == 1){
                lg.txt_user.requestFocus(true);
            }else{
                JOptionPane.showMessageDialog(null, "Saliendo");
                System.exit(0);
            }
        }else if(e.getSource() == lg.btn_Ingresar){
            if(lg.txt_Contra.getText().isEmpty() || lg.txt_user.getText().isEmpty()){
                JOptionPane.showMessageDialog(null, "Campos Vacios","Verificar Campos", JOptionPane.ERROR_MESSAGE);
            }else{
                if(verificarUser() == true){
                JOptionPane.showMessageDialog(null, "Usuario y/o Contraseña Incorrectas", "Error al Ingresar",JOptionPane.ERROR_MESSAGE);
                limpiar();
            }else{
                    ColocarCargo(Cargo, user2);
                }
            }
            
        }
     }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
     }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
         if(e.getSource() == lg.btn_Cancelar){
            lg.btn_Cancelar.setBackground(Color.red);
        }else if(e.getSource() == lg.btn_Ingresar){
            lg.btn_Ingresar.setBackground(Color.red);
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
         if(e.getSource() == lg.btn_Cancelar){
            lg.btn_Cancelar.setBackground(Color.white);
        }else if(e.getSource() == lg.btn_Ingresar){
            lg.btn_Ingresar.setBackground(Color.white);
        }
     }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
       if(e.getKeyCode() == KeyEvent.VK_ENTER){
           if(e.getSource() == lg.txt_Contra){
                if(lg.txt_Contra.getText().toString().isEmpty() || lg.txt_user.getText().isEmpty()){
                JOptionPane.showMessageDialog(null, "Campos Vacios","Verificar Campos", JOptionPane.ERROR_MESSAGE);
            }else{
                if(verificarUser() == true){
                JOptionPane.showMessageDialog(null, "Usuario y/o Contraseña Incorrectas", "Error al Ingresar",JOptionPane.ERROR_MESSAGE);
                limpiar();
               }else{
                   ColocarCargo(Cargo, user2);
                }
            }
           }
       }
    }

    @Override
    public void keyReleased(KeyEvent e) {
       
   }
    
    public boolean verificarUser(){
         try {
        String user = lg.txt_user.getText();
        String contra = lg.txt_Contra.getText().toString();
        
        
        Connection conectar = Conexion.establecerConnection();
        ResultSet rs;
        PreparedStatement ps = conectar.prepareStatement("SELECT Usuario, Cargo FROM db_Usuarios WHERE Usuario = ? AND Contraseña = ?");
        ps.setString(1, user);
        ps.setString(2, contra);
        rs = ps.executeQuery();
          while (rs.next()) {
              user2 = (rs.getString("Usuario"));  
              Cargo = (rs.getString("Cargo"));
          }
          if(user2.isEmpty()){
             return true;
          }else{
              JOptionPane.showMessageDialog(null, "Ingresando");
              return false;
          }
        } catch (Exception e) {
            System.out.println("error Verificar login "+e.getMessage());
        }
         return false;
    }
    
    public void ColocarCargo(String cargo, String user){
        Bienvenida bn = new Bienvenida();
        bn.txt_user.setText(user);
        bn.txt_cargo.setText(cargo);
        if(!(cargo.equals("Administrador"))){
            bn.jMenu1.enable(false);
            bn.setVisible(true);
            lg.dispose();
        }else if(cargo.equals("Administrador")){
             bn.jMenu1.enable(true);
             bn.setVisible(true);
             lg.dispose();
        }
        
        
    }
    
    public void limpiar(){
        lg.txt_Contra.setText("");
        lg.txt_user.setText("");
    }
    
    
    
}
