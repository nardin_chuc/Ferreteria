/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;

import Interfaz.*;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
//import javax.swing.Icon;
//import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author HP
 */
public class Bienvenida extends javax.swing.JFrame {

    
    String contraseña;
    String contraNueva;
    String nombre;
    String User;
    String Contra;
    int inicio = 001;
    
    public Bienvenida() {
        initComponents();
        this.setLocationRelativeTo(this);
        this.setTitle(" Ferreteria ");
//        fondo();
        dia();
        Mes();
        Año();
    }
    
        
    public void dia() {
        LocalDateTime dia = LocalDateTime.now();
        DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd-MM-yy");
        String fecha = dia.format(formato);txt_fecha.setText(fecha);
    }
    
      public void Mes(){
        LocalDate currentDate = LocalDate.now();
        Month month = currentDate.getMonth();
        txt_Mes.setText(""+month);
    }
    
    public void Año(){
        LocalDate currentDate = LocalDate.now();
        int month = currentDate.getYear();
        txt_AÑO.setText(""+month);
    }
    
//public void fondo() {
//        try {
//            ImageIcon wallpaper = new ImageIcon("src/Imag/fondo.jpg");
//            ImageIcon wallpaper2 = new ImageIcon("src/Imag/fondo2.jpg");
//            Icon icono = new ImageIcon(wallpaper.getImage().getScaledInstance(Label_fondo.getWidth(), Label_fondo.getHeight(), Image.SCALE_DEFAULT));
//             
//            Icon icono2 = new ImageIcon(wallpaper2.getImage().getScaledInstance(lb_fondo2.getWidth(), lb_fondo2.getHeight(), Image.SCALE_DEFAULT));
//            Label_fondo.setIcon(icono);
//            lb_fondo2.setIcon(icono2);
//            
//        } catch (Throwable e) {
//            // JOptionPane.showMessageDialog(null, "Error: " + e.getMessage());
//        }
//
//    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        Label_fondo = new javax.swing.JLabel();
        lb_fondo2 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        Usuario = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txt_user = new javax.swing.JTextField();
        txt_cargo = new javax.swing.JTextField();
        txt_fecha = new javax.swing.JTextField();
        txt_Mes = new javax.swing.JTextField();
        txt_AÑO = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        AgregarUser = new javax.swing.JMenuItem();
        AgregarProd = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("INGRESAR");
        setBackground(new java.awt.Color(255, 153, 0));

        jPanel1.setBackground(new java.awt.Color(0, 102, 102));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Comic Sans MS", 3, 48)); // NOI18N
        jLabel1.setText("FERRETERIA");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 30, 320, 110));

        Label_fondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imag/1-Mejores-marcas-de-herramienta-de-Ferreteria-2.jpg"))); // NOI18N
        jPanel1.add(Label_fondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(267, 13, 841, 269));

        lb_fondo2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imag/herramientas-de-bricolaje-848x477x80xX.jpg"))); // NOI18N
        lb_fondo2.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        jPanel1.add(lb_fondo2, new org.netbeans.lib.awtextra.AbsoluteConstraints(267, 289, -1, 388));

        jButton1.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        jButton1.setText("Cerrar Sesion");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1126, 13, 139, 42));

        Usuario.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        Usuario.setForeground(new java.awt.Color(255, 255, 255));
        Usuario.setText("Usuario: ");
        jPanel1.add(Usuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(1126, 76, -1, -1));

        jLabel2.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Cargo: ");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1126, 121, -1, -1));

        txt_user.setEditable(false);
        txt_user.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        jPanel1.add(txt_user, new org.netbeans.lib.awtextra.AbsoluteConstraints(1193, 73, 116, -1));

        txt_cargo.setEditable(false);
        txt_cargo.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        jPanel1.add(txt_cargo, new org.netbeans.lib.awtextra.AbsoluteConstraints(1194, 118, 116, -1));

        txt_fecha.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        txt_fecha.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jPanel1.add(txt_fecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(137, 169, 90, -1));

        txt_Mes.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        txt_Mes.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jPanel1.add(txt_Mes, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 169, 60, -1));

        txt_AÑO.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        txt_AÑO.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jPanel1.add(txt_AÑO, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 169, 63, -1));

        jLabel18.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setText("AÑO");
        jPanel1.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 139, -1, -1));

        jLabel19.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setText("Mes");
        jPanel1.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(88, 139, -1, -1));

        jLabel20.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setText("Fecha");
        jPanel1.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(163, 139, -1, -1));

        jMenuBar1.setBackground(new java.awt.Color(204, 0, 0));
        jMenuBar1.setForeground(new java.awt.Color(0, 102, 102));
        jMenuBar1.setFocusable(false);
        jMenuBar1.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N

        jMenu1.setBackground(new java.awt.Color(255, 51, 51));
        jMenu1.setForeground(new java.awt.Color(255, 255, 255));
        jMenu1.setText("Menu");
        jMenu1.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N

        AgregarUser.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        AgregarUser.setText("Usuarios");
        AgregarUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgregarUserActionPerformed(evt);
            }
        });
        jMenu1.add(AgregarUser);

        AgregarProd.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        AgregarProd.setText("Productos");
        AgregarProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgregarProdActionPerformed(evt);
            }
        });
        jMenu1.add(AgregarProd);

        jMenuItem2.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jMenuItem2.setText("Proveedores");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        jMenu5.setBackground(new java.awt.Color(255, 51, 51));
        jMenu5.setForeground(new java.awt.Color(255, 255, 255));
        jMenu5.setText("Compras");
        jMenu5.setFont(new java.awt.Font("Comic Sans MS", 3, 18)); // NOI18N

        jMenuItem4.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        jMenuItem4.setText("Realizar Compra");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem4);

        jMenuItem5.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        jMenuItem5.setText("Ver Compras");
        jMenu5.add(jMenuItem5);

        jMenuBar1.add(jMenu5);

        jMenu4.setBackground(new java.awt.Color(255, 51, 51));
        jMenu4.setForeground(new java.awt.Color(255, 255, 255));
        jMenu4.setText("Ventas");
        jMenu4.setFont(new java.awt.Font("Comic Sans MS", 3, 18)); // NOI18N

        jMenuItem3.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        jMenuItem3.setText("Realizar Ventas");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem3);

        jMenuItem6.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        jMenuItem6.setText("Ver Ventas");
        jMenu4.add(jMenuItem6);

        jMenuBar1.add(jMenu4);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1334, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 678, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void AgregarUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgregarUserActionPerformed
        AgregarUsuario user = new AgregarUsuario();
        user.setVisible(true);
    }//GEN-LAST:event_AgregarUserActionPerformed

    private void AgregarProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgregarProdActionPerformed
        AgregarProducto prod = new AgregarProducto();
        prod.setVisible(true);

    }//GEN-LAST:event_AgregarProdActionPerformed
    
    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
      String user = txt_user.getText();
      String Cargo = txt_cargo.getText();
      Ventas vn = new Ventas();
      vn.txt_user.setText(user);
      vn.txt_cargo.setText(Cargo);
      vn.setVisible(true);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
       IngresarProveedor pro = new IngresarProveedor();
       pro.setVisible(true);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Login lg = new Login();
        lg.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        Compras cp = new Compras();
        String user = txt_user.getText();
      String Cargo = txt_cargo.getText();
      cp.txt_user.setText(user);
      cp.txt_cargo.setText(Cargo);
        cp.setVisible(true);
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Bienvenida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Bienvenida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Bienvenida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Bienvenida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Bienvenida().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem AgregarProd;
    private javax.swing.JMenuItem AgregarUser;
    private javax.swing.JLabel Label_fondo;
    private javax.swing.JLabel Usuario;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    public javax.swing.JLabel jLabel18;
    public javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    public javax.swing.JLabel jLabel20;
    public javax.swing.JMenu jMenu1;
    public javax.swing.JMenu jMenu4;
    public javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lb_fondo2;
    public javax.swing.JTextField txt_AÑO;
    public javax.swing.JTextField txt_Mes;
    public javax.swing.JTextField txt_cargo;
    public javax.swing.JTextField txt_fecha;
    public javax.swing.JTextField txt_user;
    // End of variables declaration//GEN-END:variables
}
